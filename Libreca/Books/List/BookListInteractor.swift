//
//  BookListInteractor.swift
//  Libreca
//
//  Created by Justin Marshall on 5/7/19.
//  
//  Libreca is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  Libreca is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with Libreca.  If not, see <https://www.gnu.org/licenses/>.
//
//  Copyright © 2019 Justin Marshall
//  This file is part of project: Libreca
//

import CalibreKit
import Foundation

struct BookListInteractor: BookListInteracting {
    let dataManager: BookListDataManaging
    
    func retryFailures(from books: [BookFetchResult], progress: @escaping (BookFetchResult, Int) -> Void, completion: @escaping () -> Void) {
        dataManager.retryFailures(from: books, progress: progress, completion: completion)
    }
    
    func updateBooks(_ books: [BookFetchResult], havingChanges changes: [BookModel]) -> [BookFetchResult] {
        var newBookList = books
        for index in 0..<newBookList.count {
            if let thisBook = newBookList[index].book,
                let changedBook = changes.first(where: { $0.isEqual(to: thisBook) }) {
                newBookList.remove(at: index)
                newBookList.insert(.book(changedBook), at: index)
            }
        }
        return newBookList
    }
    
    func fetchBooks(allowCached: Bool, start: @escaping (Swift.Result<Int, FetchError>) -> Void, progress: @escaping (_ result: BookFetchResult, _ index: Int) -> Void, completion: @escaping ([BookFetchResult]) -> Void) {
        dataManager.fetchBooks(
            allowCached: allowCached, start: { result in
                start(result)
            }, progress: { result, index in
                progress(result, index)
            }, completion: { results in
                let sortedResults = self.sort(books: results, by: Settings.Sort.current)
                completion(sortedResults)
            }
        )
    }
    
    func sort(books: [BookFetchResult], by sort: Settings.Sort) -> [BookFetchResult] {
        switch sort {
        case .authorLastName:
            return books.sorted { result1, result2 in
                switch (result1, result2) {
                case (.book(let book1), .book(let book2)):
                    if book1[keyPath: sort.sortingKeyPath] != book2[keyPath: sort.sortingKeyPath] {
                        return sort.sortAction(book1, book2)
                    } else if book1.series?.name != book2.series?.name {
                        return (book1.series?.name ?? "") < (book2.series?.name ?? "")
                    } else {
                        return (book1.series?.index ?? -Double(Int.min)) < (book2.series?.index ?? -Double(Int.min))
                    }
                case (.inFlight, .book), (.failure, .book):
                    return true
                case (.book, _),
                     (.failure, .inFlight),
                     (.failure, .failure),
                     (.inFlight, .inFlight),
                     (.inFlight, .failure):
                    return false
                }
            }
        case .title:
            return books.sorted { result1, result2 in
                switch (result1, result2) {
                case (.book(let book1), .book(let book2)):
                    return sort.sortAction(book1, book2)
                case (.inFlight, .book), (.failure, .book):
                    return true
                case (.book, _),
                     (.failure, .inFlight),
                     (.failure, .failure),
                     (.inFlight, .inFlight),
                     (.inFlight, .failure):
                    return false
                }
            }
        }
    }
    
    func updateSortSetting(to newSortSetting: Settings.Sort) {
        let oldSort = Settings.Sort.current
        if oldSort != newSortSetting {
            Settings.Sort.current = newSortSetting
        }
    }
    
    func searchBooks(_ books: [BookFetchResult], usingTerms terms: String) -> [BookFetchResult] {
        let terms = terms.split(separator: " ").map(String.init)
        let dataSet = books.compactMap { $0.book }
        let searcher = Searcher(dataSet: dataSet, terms: terms)
        let matches = searcher.search()
        let matchResults = matches.map { BookFetchResult.book($0) }
        return matchResults
    }
    
    func clearImageCache() {
        Cache.clear()
    }
}
